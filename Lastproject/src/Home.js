import React from 'react'
import {Platform, View, Text, Image, ScrollView, TextInput, StyleSheet, Button,
    TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

const Home = () => {
    return (
        <View style = {styles.container}>
            <View style={styles.kotaklogin}>
                    <TextInput style = {styles.input}>
                    <Icon name = 'search' size = {25}></Icon>
                    <Text style = {styles.katasearch}> Search Product</Text>
                    <Icon name = 'camera_alt' size  = {25}></Icon>
                    </TextInput>
            </View>
            <Image source = {require('./summersales.jpg')}style = {styles.gambar}></Image>
            <View style = {styles.logo}>
                    <View style = {styles.logobajulaki}/>
                    <View style = {styles.logobajucewe}/>
                    <View style = {styles.logobajukids}/>
                    <View style = {styles.logobajuhome}/>   
                    <View style = {styles.logobajumore}/>

            </View>

        </View>
        
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    kotaklogin: {
        marginHorizontal: 30,
        marginVertical: 5,
        alignContent: 'center',
        width: 294
    },
    btreg : {
        borderColor : 'black',
        // backgroundColor : 'red',
        borderWidth : 1,
        width : 300,
        // height : 100,
    },
    textbt: {
        color: 'black',
        textAlign: "center",
        fontSize: 15,
        fontWeight: "bold",
    },
    input: {
        height: 40,
        borderColor  : 'black',
        borderWidth : 1,
        alignItems : 'center'
    },
    katasearch :{
        fontSize : 15,
        color : 'black',
        textAlign : 'center',
        // justifyContent : 'center'
       
    },
    gambar : {
        width : 400,
        height :200,
        marginTop : 20,
    },
    logo : {
        justifyContent : 'center',
        alignItems : 'center',
        marginTop : 10,
        flexDirection : 'row',
        
    },
    logobajulaki : {
        width : 52,
        height : 52,
        backgroundColor : 'red',
        borderRadius : 20,
        flexDirection : "row"
    },
    logobajucewe : {
        width : 52,
        height : 52,
        backgroundColor : '#ff7171',
        borderRadius : 20,
    },
    logobajukids : {
        width : 52,
        height : 52,
        backgroundColor : '#ff7171',
        borderRadius : 20,
    },
    logobajuhome : {
        width : 52,
        height : 52,
        backgroundColor : '#ff7171',
        borderRadius : 20,
    },
    logobajumore : {
        width : 52,
        height : 52,
        backgroundColor : '#ff7171',
        borderRadius : 20,
    },

   
})


export default Home



